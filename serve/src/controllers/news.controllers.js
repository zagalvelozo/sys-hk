var New = require("../schemas/new.model");
var Session = require("../schemas/session.model");

exports.getNews = async (req, res, next) => {
    req.session.views = (req.session.views || 0) + 1
    const sessionId = req.header('X-Token')
    if(req.session.views > 1 && sessionId ) {
      var session = await Session.findByIdAndUpdate(sessionId, {key: req.session});
      var bannedIds = session.baned_news ? session.baned_news.map(n => n._id) : [];
      New.find({_id: { $nin: bannedIds }}).exec((err, result) => {
        if (err) throw Error('Error finding news');
        res.set({ 'X-Token': session._id });
        res.status(200).json(result.map(r => ({
            id: r.id,
            story_tile: r.story_title,
            title: r.title,
            author: r.author,
            created_at: r.created_at,
            story_url: r.story_url,
            url: r.url,
        })));
      })
    } else {
      console.log('sin sesion');
      var session = new Session({ key: req.session });
      session = await session.save();
      New.find({}).exec((err, result) => {
        if (err) throw Error('Error finding news');
        res.set({ 'X-Token': session._id });
        res.status(200).json(result.map(r => ({
            id: r.id,
            story_tile: r.story_title,
            title: r.title,
            author: r.author,
            created_at: r.created_at,
            story_url: r.story_url,
            url: r.url,
        })));
      })
    }
}
exports.deleteNew = async (req, res, next) => {
  var sessionId = req.header('X-Token');
  var newId = req.params.id;
  var bannedNew = await New.findById(newId);
  Session.findByIdAndUpdate(sessionId, { $push: { baned_news: bannedNew }}).exec((err, result) => {
    if (err) throw Error('Error when updating new');
    res.set({'X-Token': sessionId});
    res.status(200).json({
      ok: true,
      baned_new: newId
    });
  });
} 