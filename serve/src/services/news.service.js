var New = require('../schemas/new.model');
const request = require("request");

exports.saveNews = async (news) => {
  const newsPromises = [];
  news.forEach(_new => {
    // console.log('verify new', _new);
    if (_new.title) {
      newsPromises.push(
        New.findOne({ title: _new.title }, (err, n) => {
          if (err) throw new Error(err);
          if (n) {
            // console.log('new already saved');
          } else {
            var _New = new New(_new);
            _New.save((err, n) => {
              if (err) console.log(err);
              console.log("insert new object: ", n);
            })
          }
        })
      )
    }
    if (_new.story_title) {
      newsPromises.push(
        New.findOne({ story_title: _new.story_title }, (err, n) => {
          if (err) throw new Error(err);
          if (n) {
            // console.log('new already saved');
          } else {
            var _New = new New(_new);
            _New.save((err, n) => {
              if (err) console.log(err);
              console.log("insert new object: ", n);
            })
          }
        })
      )
    }
  })
  return Promise.all(newsPromises).then(result => {
    console.log('news loaded');
    return Promise.resolve();
  }).catch(err => {
    console.log('Error:', err);
    return Promise.reject();
  });
}


exports.getNews = (cb) => {
  const url = 'http://hn.algolia.com/api/v1/search_by_date?query=nodejs';
  request.get(url, (error, response, body) => {
    if (error) {
      return cb(error, null);
    }
    var { hits } = JSON.parse(body);
    hits = hits.map(hit => ({
      story_title: hit.story_title,
      title: hit.title,
      author: hit.author,
      created_at: hit.created_at,
      story_url: hit.story_url,
      url: hit.url
    }));
    return cb(null, hits)
  });
}


